const pwForm = document.querySelector(".password-form")
pwForm.addEventListener('click', (ev) => {
    const target = ev.target.tagName;
    if (target === "I") {
        if (ev.target.classList.contains("fa-eye")) {
            ev.target.classList.remove('fa-eye');
            ev.target.classList.add("fa-eye-slash");
        } else {
            ev.target.classList.remove("fa-eye-slash");
            ev.target.classList.add('fa-eye');
        }


        const pw = ev.target.previousElementSibling;

        if (pw.getAttribute("type") === "password") {
            pw.setAttribute("type", "text");
        } else {
            pw.setAttribute("type", "password");
        }
    }
})

const btn = document.querySelector(".btn");
btn.addEventListener("click",submit);
const error = document.createElement("p");
error.style.color = "red";
btn.insertAdjacentElement("beforebegin", error);

function submit(ev) {
    ev.preventDefault();
    const firstPW = document.getElementById("pw1");
    const secondPW = document.getElementById("pw2");

if(firstPW.value && secondPW.value && firstPW.value === secondPW.value) {
    alert("You are welcome");
    error.textContent = "";
}
else {
error.textContent = "Потрібно ввести однакові значення";
}
}


